$(window).on('load', function () {
$("#top-stories-body").owlCarousel({
        items: 1,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoplayHoverPause: true,
        nav: true,
        dots: true,
        navText: ["<i class='lni lni-chevron-left'></i>", "<i class='lni lni-chevron-right'></i>"],
        responsive: {
            0: {
                items: 1,
            },
            480: {
                items: 1
            }
        }
    });
    
    $(".news").owlCarousel({
        items: 1,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoplayHoverPause: true,
        nav: true,
        dots: false,
        navText: ["<i class='fas fa-chevron-circle-left'></i>", "<i class='fas fa-chevron-circle-right'></i>"],
        responsive: {
            0: {
                items: 1,
            },
            480: {
                items: 1
            }
        }
    });
});